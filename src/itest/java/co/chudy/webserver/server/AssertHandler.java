package co.chudy.webserver.server;

import java.util.Arrays;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;

public class AssertHandler implements ResponseHandler<Void> {

  private final AssertResponse[] assertResponse;

  AssertHandler(AssertResponse... assertResponse) {
    this.assertResponse = assertResponse;
  }

  @Override
  public Void handleResponse(HttpResponse response) {
    Arrays.stream(assertResponse)
        .forEach(r -> r.assertThat(response));
    return null;
  }
}
