package co.chudy.webserver.server;

import org.apache.http.HttpResponse;

interface AssertResponse {

  void  assertThat(HttpResponse response);

}
