package co.chudy.webserver.server;

import static co.chudy.webserver.util.MimeTypes.JS_MIME_TYPE;
import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsEmpty;
import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsServedFrom;
import static co.chudy.webserver.server.ResponseAssertions.assertThatHeaderIsPresent;
import static co.chudy.webserver.server.ResponseAssertions.assertThatMimeTypeIs;
import static co.chudy.webserver.server.ResponseAssertions.assertThatResponseCodeIs;
import static org.apache.http.HttpStatus.SC_NOT_MODIFIED;
import static org.apache.http.HttpStatus.SC_OK;

import co.chudy.webserver.util.ServerRunner;
import java.io.IOException;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ServerRunner.class)
public class EtagCachingTest {

  private static final String HTML_5_SHIV_JS_PATH = "src/itest/resources/docroot/javascripts/html5shiv.js";
  private static final String URL = "http://localhost:8080/javascripts/html5shiv.js";

  private static String etagHeader;
  
  private static String lastModifiedHeader;

  @BeforeAll
  public static void initialRequest() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      // TODO: I was thinking whether this is OK to keep assertions in preparation method
      // but after all it does not hurt and brings some value altough it's not kosher
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsServedFrom(HTML_5_SHIV_JS_PATH),
              assertThatHeaderIsPresent(HttpHeaders.ETAG),
              response -> etagHeader = response.getFirstHeader(HttpHeaders.ETAG).getValue(),
              response -> lastModifiedHeader = response.getFirstHeader(HttpHeaders.LAST_MODIFIED).getValue()
          )
      );
    }
  }

  @DisplayName("Matching Etag should result in 304 response with empty body.")
  @Test
  public void testMatchingEtag() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, etagHeader);
      //TODO: I was considering extracting this fragment of code as parametrized method, but
      // it would be at the expense of readability. It's much more clear this way.
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_NOT_MODIFIED),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

  @DisplayName("If-Modified-Since should be ignored, when If-None-Match is present and matches")
  @Test
  public void testPrecedenceOfEtagMatchOverIfModifiedSince() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, etagHeader);
      httpGet.setHeader(HttpHeaders.IF_MODIFIED_SINCE, lastModifiedHeader);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_NOT_MODIFIED),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

  @DisplayName("If-Modified-Since should be ignored, when If-None-Match is present and does not match")
  @Test
  public void testPrecedenceOfEtagMismatchOverIfModifiedSince() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, etagHeader + "x");
      httpGet.setHeader(HttpHeaders.IF_MODIFIED_SINCE, lastModifiedHeader);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsServedFrom(HTML_5_SHIV_JS_PATH)
          )
      );
    }
  }

  @DisplayName("Non-matching Etag should result in 200 response with body.")
  @Test
  public void testNonMatchingEtag() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, etagHeader + "x");
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsServedFrom(HTML_5_SHIV_JS_PATH)
          )
      );
    }
  }

  @DisplayName("Matching one of many Etags should result in 304 response with empty body.")
  @Test
  public void testMatchingEtagOneOfMany() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, String.format("xyzzy, %s, c3piozzzz", etagHeader));
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_NOT_MODIFIED),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

  @DisplayName("Matching * should result in 304 response with empty body.")
  @Test
  public void testMatchingStar() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_NONE_MATCH, "*");
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_NOT_MODIFIED),
              assertThatMimeTypeIs(JS_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

}
