package co.chudy.webserver.server;

import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsEmpty;
import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsServedFrom;
import static co.chudy.webserver.server.ResponseAssertions.assertThatHeaderIsPresent;
import static co.chudy.webserver.server.ResponseAssertions.assertThatMimeTypeIs;
import static co.chudy.webserver.server.ResponseAssertions.assertThatResponseCodeIs;
import static org.apache.http.HttpStatus.SC_NOT_MODIFIED;
import static org.apache.http.HttpStatus.SC_OK;

import co.chudy.webserver.util.MimeTypes;
import co.chudy.webserver.util.ServerRunner;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ServerRunner.class)
public class LastModifiedTest {

  private static final String INDEX_HTML_PATH = "src/itest/resources/docroot/index.html";

  private static final String URL = "http://localhost:8080";

  private static String lastModifiedHeader;

  private static ZonedDateTime lastModified;

  @BeforeAll
  public static void initialRequest() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom(INDEX_HTML_PATH),
              assertThatHeaderIsPresent(HttpHeaders.LAST_MODIFIED),
              response -> lastModifiedHeader = response.getFirstHeader(HttpHeaders.LAST_MODIFIED).getValue()
          )
      );
      lastModified = ZonedDateTime.parse(lastModifiedHeader, DateTimeFormatter.RFC_1123_DATE_TIME);
    }
  }

  @DisplayName("When If-Modified-Since is equal to Last-Modified, server should return 304 with empty body")
  @Test
  public void testIfModifiedSinceEqualToLastModified() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      httpGet.setHeader(HttpHeaders.IF_MODIFIED_SINCE, lastModifiedHeader);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom(INDEX_HTML_PATH)
          )
      );
    }
  }

  @DisplayName("When If-Modified-Since is later than Last-Modified, server should return 304 with empty body")
  @Test
  public void testIfModifiedSinceLaterThanToLastModified() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      String lastModifiedPlusDay = lastModified.plusDays(1).format(DateTimeFormatter.RFC_1123_DATE_TIME);
      httpGet.setHeader(HttpHeaders.IF_MODIFIED_SINCE, lastModifiedPlusDay);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_NOT_MODIFIED),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

  @DisplayName("When If-Modified-Since is earlier than Last-Modified, server should return 200")
  @Test
  public void testIfModifiedSinceEarlierThanToLastModified() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet(URL);
      String lastModifiedMinusDay = lastModified.minusDays(1).format(DateTimeFormatter.RFC_1123_DATE_TIME);
      httpGet.setHeader(HttpHeaders.IF_MODIFIED_SINCE, lastModifiedMinusDay);
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom(INDEX_HTML_PATH)
          )
      );
    }
  }
}
