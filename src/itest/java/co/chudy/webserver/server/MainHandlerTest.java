package co.chudy.webserver.server;

import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsEmpty;
import static co.chudy.webserver.server.ResponseAssertions.assertThatBodyIsServedFrom;
import static co.chudy.webserver.server.ResponseAssertions.assertThatMimeTypeIs;
import static co.chudy.webserver.server.ResponseAssertions.assertThatResponseCodeIs;
import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_OK;

import co.chudy.webserver.util.MimeTypes;
import co.chudy.webserver.util.ServerRunner;
import java.io.IOException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ServerRunner.class)
public class MainHandlerTest {

  private static final String HTML_5_SHIV_JS_PATH = "src/itest/resources/docroot/javascripts/html5shiv.js";

  private static final String INDEX_HTML_PATH = "src/itest/resources/docroot/index.html";

  @DisplayName("Check that default index discovery works.")
  @Test
  void testImplicitIndexPage() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpget = new HttpGet("http://localhost:8080");
      httpclient.execute(httpget, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom(INDEX_HTML_PATH)
          )
      );
    }
  }

  @DisplayName("Check serving a simple html file.")
  @Test
  public void testExplicitIndexPage() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpget = new HttpGet("http://localhost:8080/index.html");
      httpclient.execute(httpget, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom("src/itest/resources/docroot/index.html")
          )
      );
    }
  }

  @DisplayName("Check that 404 works and docroot file has precedence.")
  @Test
  public void testNotFound() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpget = new HttpGet("http://localhost:8080/non-existing-page.html");
      httpclient.execute(httpget, new AssertHandler(
              assertThatResponseCodeIs(404),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom("src/itest/resources/docroot/404.html")
          )
      );
    }
  }

  @DisplayName("Check that 501 works and is served from classpath as a fallback.")
  @Test
  public void testNotImplemented() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpPost httpPost = new HttpPost("http://localhost:8080");
      httpclient.execute(httpPost, new AssertHandler(
              assertThatResponseCodeIs(501),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom("src/main/resources/501.html")
          )
      );
    }
  }

  @DisplayName("Check that JS files are served with correct mime type.")
  @Test
  public void testMimeType() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpGet = new HttpGet("http://localhost:8080/javascripts/html5shiv.js");
      httpclient.execute(httpGet, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.JS_MIME_TYPE),
              assertThatBodyIsServedFrom(HTML_5_SHIV_JS_PATH)
          )
      );
    }
  }

  @DisplayName("Check that 403 works when requesting directory without an index file.")
  @Test
  public void testAccessForbidden() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpget = new HttpGet("http://localhost:8080/javascripts");
      httpclient.execute(httpget, new AssertHandler(
              assertThatResponseCodeIs(SC_FORBIDDEN),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsServedFrom("src/main/resources/403.html")
          )
      );
    }
  }

  @DisplayName("Check that HEAD request returns empty body.")
  @Test
  public void testHeadOK() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpHead httphead = new HttpHead("http://localhost:8080");
      httpclient.execute(httphead, new AssertHandler(
              assertThatResponseCodeIs(SC_OK),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

  @DisplayName("Check that HEAD request returns empty body with error status code.")
  @Test
  public void testHeadForbidden() throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpHead httphead = new HttpHead("http://localhost:8080/javascripts");
      httpclient.execute(httphead, new AssertHandler(
              assertThatResponseCodeIs(SC_FORBIDDEN),
              assertThatMimeTypeIs(MimeTypes.HTML_MIME_TYPE),
              assertThatBodyIsEmpty()
          )
      );
    }
  }

}