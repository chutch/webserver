package co.chudy.webserver.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;

class ResponseAssertions {

  static AssertResponse assertThatResponseCodeIs(int expectedResponseCode) {
    return r -> assertEquals(expectedResponseCode, r.getStatusLine().getStatusCode());
  }

  static AssertResponse assertThatMimeTypeIs(String expectedMimeType) {
    return r -> assertEquals(expectedMimeType, r.getFirstHeader(HttpHeaders.CONTENT_TYPE).getValue());
  }

  static AssertResponse assertThatHeaderIsPresent(String expectedHeader) {
    return r -> assertNotNull(r.getFirstHeader(expectedHeader));
  }


  static AssertResponse assertThatBodyIsServedFrom(String expectedContentPath) {
    return response -> compareBodyResponseToFileContent(expectedContentPath, response);
  }

  static AssertResponse assertThatBodyIsEmpty() {
    return response -> assertNull(response.getEntity());
  }

  private static void compareBodyResponseToFileContent(String expectedContentPath, HttpResponse response) {
    File patternFile = new File(expectedContentPath);
    boolean contentEquals = false;
    if (patternFile.exists() && patternFile.canRead()) {
      try {
        FileInputStream expectedStream = new FileInputStream(patternFile);
        InputStream actualStream = response.getEntity().getContent();
        contentEquals = IOUtils.contentEquals(expectedStream, actualStream);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    assertTrue(contentEquals);
  }

}
