package co.chudy.webserver.util;

import static org.apache.http.HttpStatus.SC_OK;

import co.chudy.webserver.Launcher;
import java.io.File;
import java.io.IOException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class ServerRunner implements BeforeAllCallback {

  protected static Boolean initialised = false;

  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    synchronized (initialised) {
      if (!initialised) {
        startServer();
        waitForServerToBeReady();
        initialised = true;
      }
    }
  }

  static void startServer() {
    String docroot = new File("src/itest/resources/docroot").getAbsolutePath();
    Thread thread = new Thread(() -> {
      try {
        Launcher.main(new String[]{"-root", docroot, "-port", "8080"});
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
    thread.start();
  }


  private static void waitForServerToBeReady() throws IOException, InterruptedException {
    int statusCode = 0;
    int numberOfRetries = 0;
    while (statusCode != SC_OK && numberOfRetries < 10) {
      try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
        HttpGet httpget = new HttpGet("http://localhost:8080");
        statusCode = httpclient.execute(httpget, response -> response.getStatusLine().getStatusCode());
      } catch (HttpHostConnectException e) {
        System.err.println("Server not ready, retrying in 1 sec...");
      }
      Thread.sleep(1000);
      numberOfRetries++;
    }
  }

}