package co.chudy.webserver;

import java.io.File;
import org.apache.commons.lang3.StringUtils;
import org.kohsuke.args4j.Option;

public class Config {

    private static final char[] ILLEGAL_CHARACTERS = { '/', '\n', '\r', '\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' };
    private static final String WRONG_PORT_MESSAGE = "Port has to be from range between 1 and 65535.";

    @Option(name = "-port", metaVar = "<port>", usage = "Listener port number. (default 8080)")
    private String port = "8080";

    @Option(name = "-root", required = true, metaVar = "<root>", usage = "Root directory for serving files.")
    private String rootDirectory = "";

    @Option(name = "-index", metaVar = "<index>", usage = "Index file name. (default index.html)")
    private String indexFileName = "index.html";

    Config() {
        super();
    }

    Config(String port, String rootDirectory, String indexFileName) {
        this.port = port;
        this.rootDirectory = rootDirectory;
        this.indexFileName = indexFileName;
    }

    public int getPort() {
        return Integer.parseUnsignedInt(port);
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    public String getIndexFileName() {
        return indexFileName;
    }

    void validate() throws ValidationException {
        validatePort();
        validateIndexFileName();
        validateRootDirectory();
    }

    private void validateRootDirectory() throws ValidationException {
        File docroot = new File(rootDirectory);
        if (!docroot.exists()) {
            throw new ValidationException("Root directory does not exist.");
        }
        if (!docroot.canRead()) {
            throw new ValidationException("No access to root directory.");
        }
        if (docroot.isFile()) {
            throw new ValidationException("Root directory is not a directory.");
        }
    }

    private void validateIndexFileName() throws ValidationException {
        if (StringUtils.isBlank(indexFileName) || StringUtils.containsAny(indexFileName, ILLEGAL_CHARACTERS)) {
            throw new ValidationException("Index file name is not valid.");
        }
    }

    private void validatePort() throws ValidationException {
        try {
            int intPort = Integer.parseUnsignedInt(port);
            if (intPort < 1 || intPort > 65535) {
                throw new ValidationException(WRONG_PORT_MESSAGE);
            }
        } catch (NumberFormatException e) {
            throw new ValidationException(WRONG_PORT_MESSAGE, e);
        }
    }


}
