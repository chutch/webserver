package co.chudy.webserver;

import co.chudy.webserver.server.HttpFileServer;
import java.io.ByteArrayOutputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class Launcher {

  private static final Logger logger = LogManager.getLogger(Launcher.class);
  private static final Marker MARKER_CONSOLE = MarkerManager.getMarker("CONSOLE");

  public static void main(String[] args) throws Exception {
    logger.info(MARKER_CONSOLE, "Starting server application.");
    Config config = new Config();
    CmdLineParser parser = new CmdLineParser(config);
    try {
      parser.parseArgument(args);
      config.validate();
      logger.info(MARKER_CONSOLE, "Server has started. Listening on {}...", config.getPort());
      new HttpFileServer(config).start();
    } catch (CmdLineException | ValidationException e) {
      logger.printf(Level.ERROR, MARKER_CONSOLE, "Error: %s%n", e.getMessage());
      logger.error(MARKER_CONSOLE, "Usage: <options>");
      logger.error(MARKER_CONSOLE, getUsageInstructions(parser));
      logger.printf(Level.ERROR, MARKER_CONSOLE, "Error: %s%n", e.getMessage());
      logger.printf(Level.ERROR, MARKER_CONSOLE, "%nExample: -root C:\\document\\root -port 80");
      System.exit(1);
    } catch (IllegalArgumentException e) {
      logger.error(MARKER_CONSOLE, "Error: ${e.message}\n");
      System.exit(1);
    }
  }

  private static String getUsageInstructions(CmdLineParser parser) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    parser.printUsage(outputStream);
    return outputStream.toString();
  }
}




