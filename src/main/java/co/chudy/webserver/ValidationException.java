package co.chudy.webserver;

class ValidationException extends Exception {

  ValidationException(String message) {
    super(message);
  }

  ValidationException(String message, Throwable e) {
    super(message, e);
  }
}
