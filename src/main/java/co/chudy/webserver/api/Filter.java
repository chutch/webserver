package co.chudy.webserver.api;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;

public interface Filter {

  void filter(HttpRequest request, HttpResponse response, HttpContext context, FilterChain chain)
      throws IOException, HttpException;
}
