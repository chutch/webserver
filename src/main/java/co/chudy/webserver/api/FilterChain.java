package co.chudy.webserver.api;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

public interface FilterChain {

  void setHandler(HttpRequestHandler handler);

  void setErrorHandler(HttpRequestHandler errorHandler);

  void add(Filter filter);

  void doFilter(HttpRequest request, HttpResponse response, HttpContext context) throws IOException, HttpException;

  // TODO: not really sure whether this fits here, I'd consider to move it other place
  void handleError(HttpRequest request, HttpResponse response, HttpContext context, int statusCode) throws IOException, HttpException;
}
