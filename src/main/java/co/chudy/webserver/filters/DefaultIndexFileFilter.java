package co.chudy.webserver.filters;

import co.chudy.webserver.api.Filter;
import co.chudy.webserver.api.FilterChain;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.protocol.HttpContext;

public class DefaultIndexFileFilter implements Filter {

  private String docRoot;

  private String indexFileName;

  public DefaultIndexFileFilter(String docRoot, String indexFileName) {
    this.docRoot = docRoot;
    this.indexFileName = indexFileName;
  }

  @Override
  public void filter(HttpRequest request, HttpResponse response, HttpContext context, FilterChain chain) throws IOException, HttpException {
    final File file = getFileFromRequest(request);
    if (file.isDirectory()) {
      File indexFile = new File(file, indexFileName);
      if (indexFile.exists() && indexFile.canRead()) {
        // TODO: I'm not 100% sure whether this wrapper is the best way to approach it.
        // maybe new request should be virtually dispatched. I would re-think it.
        IndexFileRequestWrapper wrapper = new IndexFileRequestWrapper(request, indexFileName);
        chain.doFilter(wrapper, response, context);
      } else {
        chain.handleError(request, response, context, HttpStatus.SC_FORBIDDEN);
      }
    } else {
      chain.doFilter(request, response, context);
    }
  }

  // TODO: these 2 lines of code are repeated, I was not sure how to deal with it quickly
  // I didn't want to use abstraction. I don't like Utility classes as well.
  // I also considered passing variables through `context`, but that would introduce unnecessary coupling between filters
  // and would also not work well with current wrapping idea. (see previous comment)
  private File getFileFromRequest(HttpRequest request) throws UnsupportedEncodingException {
    String target = request.getRequestLine().getUri();
    return new File(this.docRoot, URLDecoder.decode(target, StandardCharsets.UTF_8.name()));
  }

}
