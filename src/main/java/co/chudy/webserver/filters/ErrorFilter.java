package co.chudy.webserver.filters;

import co.chudy.webserver.api.Filter;
import co.chudy.webserver.api.FilterChain;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.protocol.HttpContext;

public class ErrorFilter implements Filter {

  private String docRoot;

  public ErrorFilter(String docRoot) {
    this.docRoot = docRoot;
  }

  @Override
  public void filter(HttpRequest request, HttpResponse response, HttpContext context, FilterChain chain) throws IOException, HttpException {
    File file = getFileFromRequest(request);
    if (!file.exists()) {
      chain.handleError(request, response, context, HttpStatus.SC_NOT_FOUND);
    } else if (!file.canRead()) {
      chain.handleError(request, response, context, HttpStatus.SC_FORBIDDEN);
    } else {
      chain.doFilter(request, response, context);
    }
  }

  private File getFileFromRequest(HttpRequest request) throws UnsupportedEncodingException {
    String target = request.getRequestLine().getUri();
    return new File(this.docRoot, URLDecoder.decode(target, StandardCharsets.UTF_8.name()));
  }

}
