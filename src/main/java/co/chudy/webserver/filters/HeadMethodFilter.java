package co.chudy.webserver.filters;

import co.chudy.webserver.api.Filter;
import co.chudy.webserver.api.FilterChain;
import co.chudy.webserver.util.HttpMethod;
import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;

public class HeadMethodFilter implements Filter {

  @Override
  public void filter(HttpRequest request, HttpResponse response, HttpContext context, FilterChain chain) throws IOException, HttpException {
    chain.doFilter(request, response, context);
    HttpMethod from = HttpMethod.from(request);
    if (from.isHead()) {
      response.setEntity(null);
    }
  }
}
