package co.chudy.webserver.filters;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpRequest;
import org.apache.http.ProtocolVersion;
import org.apache.http.RequestLine;
import org.apache.http.params.HttpParams;

class IndexFileRequestWrapper implements HttpRequest{

  private HttpRequest request;
  private String indexFileName;

  IndexFileRequestWrapper(HttpRequest request, String indexFileName) {
    this.request = request;
    this.indexFileName = indexFileName;
  }

  @Override
  public RequestLine getRequestLine() {
    return new RequestLineWrapper(request.getRequestLine(), indexFileName);
  }

  @Override
  public ProtocolVersion getProtocolVersion() {
    return request.getProtocolVersion();
  }

  @Override
  public boolean containsHeader(String name) {
    return request.containsHeader(name);
  }

  @Override
  public Header[] getHeaders(String name) {
    return request.getHeaders(name);
  }

  @Override
  public Header getFirstHeader(String name) {
    return request.getFirstHeader(name);
  }

  @Override
  public Header getLastHeader(String name) {
    return request.getLastHeader(name);
  }

  @Override
  public Header[] getAllHeaders() {
    return request.getAllHeaders();
  }

  @Override
  public void addHeader(Header header) {
    request.addHeader(header);
  }

  @Override
  public void addHeader(String name, String value) {
    request.addHeader(name, value);
  }

  @Override
  public void setHeader(Header header) {
    request.setHeader(header);
  }

  @Override
  public void setHeader(String name, String value) {
    request.setHeader(name, value);
  }

  @Override
  public void setHeaders(Header[] headers) {
    request.setHeaders(headers);
  }

  @Override
  public void removeHeader(Header header) {
    request.removeHeader(header);
  }

  @Override
  public void removeHeaders(String name) {
    request.removeHeaders(name);
  }

  @Override
  public HeaderIterator headerIterator() {
    return request.headerIterator();
  }

  @Override
  public HeaderIterator headerIterator(String name) {
    return request.headerIterator(name);
  }

  @Override
  @Deprecated
  public HttpParams getParams() {
    return request.getParams();
  }

  @Override
  @Deprecated
  public void setParams(HttpParams params) {
    request.setParams(params);
  }

  private class RequestLineWrapper implements RequestLine {

    private RequestLine requestLine;
    private String indexFileName;

    RequestLineWrapper(RequestLine requestLine, String indexFileName) {
      this.requestLine = requestLine;
      this.indexFileName = indexFileName;
    }

    @Override
    public String getMethod() {
      return requestLine.getMethod();
    }

    @Override
    public ProtocolVersion getProtocolVersion() {
      return requestLine.getProtocolVersion();
    }

    @Override
    public String getUri() {
      return requestLine.getUri() + "/" + indexFileName;
    }
  }
}
