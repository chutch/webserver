package co.chudy.webserver.filters;

import static org.apache.http.HttpStatus.SC_NOT_IMPLEMENTED;

import co.chudy.webserver.api.Filter;
import co.chudy.webserver.api.FilterChain;
import co.chudy.webserver.util.HttpMethod;
import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;

public class MethodNotSupportedFilter implements Filter {

  @Override
  public void filter(HttpRequest request, HttpResponse response, HttpContext context, FilterChain chain) throws IOException, HttpException {
    HttpMethod method = HttpMethod.from(request);
    if (method.isSupported()) {
      chain.doFilter(request, response, context);
    } else {
      chain.handleError(request, response, context, SC_NOT_IMPLEMENTED);
    }
  }
}
