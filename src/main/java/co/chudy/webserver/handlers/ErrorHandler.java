package co.chudy.webserver.handlers;

import co.chudy.webserver.util.MimeTypes;
import java.io.File;
import java.io.InputStream;
import java.util.Optional;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

public class ErrorHandler implements HttpRequestHandler {

  static final String STATUS_CODE = "statusCode";

  private final String docRoot;

  ErrorHandler(final String docRoot) {
    super();
    this.docRoot = docRoot;
  }

  public void handle(final HttpRequest request, final HttpResponse response, final HttpContext context) {
    Optional<Integer> statusCode = Optional.of((Integer) context.getAttribute(STATUS_CODE));
    // TODO: didn't want to introduce new interface for Error Handler, so I decided to reuse existing one
    // and use convention with `context` here. I would reconsider it now, especially in combination with comment from FilterChain class
    handleError(response, statusCode.get());
  }

  private void handleError(HttpResponse response, int statusCode) {
    HttpEntity entity = getErrorPageFromDocrootOrResources(statusCode);
    response.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, MimeTypes.HTML_MIME_TYPE));
    response.setEntity(entity);
    response.setStatusCode(statusCode);
  }

  private HttpEntity getErrorPageFromDocrootOrResources(int statusCode) {
    String errorPageFileName = String.format("/%d.html", statusCode);
    File errorPageFile = new File(this.docRoot, errorPageFileName);
    boolean errorPageAvailableInDocroot = errorPageFile.exists() && errorPageFile.canRead();
    return errorPageAvailableInDocroot ? new FileEntity(errorPageFile) : getErrorPageFromClasspath(errorPageFileName);
  }

  private InputStreamEntity getErrorPageFromClasspath(String filename) {
    InputStream resourceAsStream = ErrorHandler.class.getResourceAsStream(filename);
    return new InputStreamEntity(resourceAsStream);
  }

}