package co.chudy.webserver.handlers;

import static java.time.ZoneOffset.UTC;

import co.chudy.webserver.util.MimeTypes;
import com.google.common.hash.Hashing;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.FileEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.overviewproject.mime_types.GetBytesException;
import org.overviewproject.mime_types.MimeTypeDetector;

public class FileHandler implements HttpRequestHandler {

  private static final Logger logger = LogManager.getLogger(FileHandler.class);

  private static final MimeTypeDetector MIME_TYPE_DETECTOR = new MimeTypeDetector();

  private String docRoot;

  FileHandler(String root) {
    super();
    docRoot = root;
  }

  @Override
  public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws IOException {
    String target = request.getRequestLine().getUri();
    final File file = new File(this.docRoot, URLDecoder.decode(target, StandardCharsets.UTF_8.name()));
    handleFileRequest(request, response, file);
  }

  private void handleFileRequest(HttpRequest request, HttpResponse response, File file) {
    String etag = getHash(file);
    String mimeType = getMimeType(file);
    ZonedDateTime lastModified = getLastModified(file);

    response.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, mimeType));

    Boolean clientHasCachedVersion = doesClientHasCachedVersion(request, etag, lastModified);

    if (clientHasCachedVersion) {
      response.setStatusCode(HttpStatus.SC_NOT_MODIFIED);
    } else {
      response.setEntity(new FileEntity(file));
      response.setStatusCode(HttpStatus.SC_OK);
      response.setHeader(HttpHeaders.ETAG, etag);
      response.setHeader(HttpHeaders.LAST_MODIFIED, lastModified.format(DateTimeFormatter.RFC_1123_DATE_TIME));
      // TODO: I'd move this option to server configuration, preferably per file type
      // no-cache is set to enable testing server in a browser
      response.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache");
    }
  }

  private Boolean doesClientHasCachedVersion(HttpRequest request, String etag, ZonedDateTime lastModified) {
    Optional<Boolean> etagMatches = doesEtagMatch(request, etag);
    Optional<Boolean> modifiedSince = isModifiedSince(request, lastModified);
    return etagMatches.orElse(modifiedSince.orElse(false));
  }

  private Optional<Boolean> doesEtagMatch(HttpRequest request, String etag) {
    Optional<Header> header = getHeader(request, HttpHeaders.IF_NONE_MATCH);
    Optional<HeaderElement[]> elements = header.map(Header::getElements);
    Optional<Stream<HeaderElement>> elementsStream = elements.map(Arrays::stream);
    return elementsStream.map(stream -> stream.anyMatch(e -> e.getName().equals(etag) || e.getName().equals("*")));
  }

  private Optional<Boolean> isModifiedSince(HttpRequest request, ZonedDateTime lastModified) {
    return getIfModifiedSinceHeader(request).map(ifModifiedSince -> ifModifiedSince.compareTo(lastModified) > 0);
  }

  private ZonedDateTime getLastModified(File file) {
    return ZonedDateTime.ofInstant(new Date(file.lastModified()).toInstant(), UTC);
  }

  private String getHash(File file) {
    long length = file.length();
    long lastModified = file.lastModified();
    // I was considering different algorithms here. MD5 is used by AWS, seems to be fast and has decent collision risk.
    // MD5 is deprecated in this library and crc32c seems to be cheap and good enough
    return Hashing.crc32c().newHasher().putLong(lastModified).putLong(length).hash().toString();
  }

  private String getMimeType(File file) {
    String mimeType = MimeTypes.HTML_MIME_TYPE;
    try {
      mimeType = MIME_TYPE_DETECTOR.detectMimeType(file);
    } catch (GetBytesException e) {
      logger.error("Can not detect the mime type by reading file.", e);
    }
    return mimeType;
  }

  private Optional<ZonedDateTime> getIfModifiedSinceHeader(HttpRequest request) {
    Optional<Header> dateHeader = getHeader(request, HttpHeaders.IF_MODIFIED_SINCE);
    return dateHeader.map(h -> ZonedDateTime.parse(h.getValue(), DateTimeFormatter.RFC_1123_DATE_TIME));
  }

  private Optional<Header> getHeader(HttpRequest request, String header) {
    return Optional.ofNullable(request.getFirstHeader(header));
  }


}
