package co.chudy.webserver.handlers;

import co.chudy.webserver.api.Filter;
import co.chudy.webserver.api.FilterChain;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

class FilterChainImpl implements FilterChain {

  private List<Filter> filters = new ArrayList<>();

  private Iterator<Filter> iterator;

  private HttpRequestHandler handler;

  private HttpRequestHandler errorHandler;

  @Override
  public void setHandler(HttpRequestHandler handler) {
    this.handler = handler;
  }

  @Override
  public void setErrorHandler(HttpRequestHandler errorHandler) {
    this.errorHandler = errorHandler;
  }

  @Override
  public void add(Filter filter) {
    filters.add(filter);
  }

  @Override
  public void doFilter(HttpRequest request, HttpResponse response, HttpContext context) throws IOException, HttpException {
    if (iterator == null) {
      iterator = filters.iterator();
      seal();
    }
    if (iterator.hasNext()) {
      iterator.next().filter(request, response, context, this);
    } else {
      handler.handle(request, response, context);
    }
  }

  private void seal() {
    filters = Collections.unmodifiableList(filters);
  }

  @Override
  public void handleError(HttpRequest request, HttpResponse response, HttpContext context, int statusCode) throws IOException, HttpException {
    context.setAttribute(ErrorHandler.STATUS_CODE, statusCode);
    errorHandler.handle(request, response, context);
  }
}
