package co.chudy.webserver.handlers;

import co.chudy.webserver.filters.DefaultIndexFileFilter;
import co.chudy.webserver.filters.ErrorFilter;
import co.chudy.webserver.filters.HeadMethodFilter;
import co.chudy.webserver.filters.MethodNotSupportedFilter;
import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

//TODO: General note regarding packages: I'm not a big fan of dividing classes into packages by type.
// I prefer division by responsibility, I would definitely want to rethink that.

public class MainHandler implements HttpRequestHandler {

  private final String docRoot;

  private final String indexFileName;

  public MainHandler(final String docRoot, final String indexFileName) {
    super();
    this.docRoot = docRoot;
    this.indexFileName = indexFileName;
  }

  public void handle(final HttpRequest request, final HttpResponse response, final HttpContext context) throws IOException, HttpException {
    FilterChainImpl filterChain = new FilterChainImpl();
    filterChain.setErrorHandler(new ErrorHandler(docRoot));
    filterChain.add(new MethodNotSupportedFilter());
    filterChain.add(new ErrorFilter(docRoot));
    filterChain.add(new DefaultIndexFileFilter(docRoot, indexFileName));
    filterChain.add(new HeadMethodFilter());
    filterChain.setHandler(new FileHandler(docRoot));
    filterChain.doFilter(request, response, context);
  }

}