package co.chudy.webserver.server;

import co.chudy.webserver.Config;
import co.chudy.webserver.handlers.MainHandler;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.DefaultBHttpServerConnectionFactory;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.bootstrap.HttpServer;
import org.apache.http.impl.bootstrap.ServerBootstrap;

public class HttpFileServer {

  private static final String SERVER_INFO = "Homework/1.0";

  private static final String MATCH_ALL_PATTERN = "*";

  private final HttpServer server;

  public HttpFileServer(Config config) {

    SocketConfig socketConfig = SocketConfig.custom()
        .setSoTimeout(15000)
        .setTcpNoDelay(true)
        .build();

    MainHandler handler = new MainHandler(config.getRootDirectory(), config.getIndexFileName());

    server = ServerBootstrap.bootstrap()
        .setListenerPort(config.getPort())
        .setServerInfo(SERVER_INFO)
        .setSocketConfig(socketConfig)
        .setSslContext(null)
        .setExceptionLogger(new StdErrorExceptionLogger())
        .registerHandler(MATCH_ALL_PATTERN, handler)
        .addInterceptorLast(new RequestLoggerInterceptor())
        .setConnectionReuseStrategy(new DefaultConnectionReuseStrategy())
        .setConnectionFactory(new DefaultBHttpServerConnectionFactory())
        .create();
  }

  public void start() throws IOException, InterruptedException {
    server.start();
    server.awaitTermination(Integer.MAX_VALUE, TimeUnit.DAYS);
  }
}