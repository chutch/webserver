package co.chudy.webserver.server;

import java.io.IOException;
import org.apache.http.HttpConnection;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.RequestLine;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.message.FormattedMessage;

class RequestLoggerInterceptor implements HttpResponseInterceptor {

  private static final Marker MARKER_REQUEST = MarkerManager.getMarker("REQUEST");

  private static final Logger logger = LogManager.getLogger(RequestLoggerInterceptor.class);

  @Override
  public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
    HttpCoreContext coreContext = HttpCoreContext.adapt(context);
    HttpRequest request = coreContext.getRequest();
    logRequest(request, response, coreContext);
  }

  private void logRequest(HttpRequest request, HttpResponse response, HttpCoreContext coreContext) {
    HttpConnection connection = coreContext.getConnection(HttpConnection.class);
    int statusCode = response.getStatusLine().getStatusCode();
    RequestMessage requestMessage = new RequestMessage(statusCode, request.getRequestLine(), connection);
    logger.info(MARKER_REQUEST, requestMessage);
  }

  static class RequestMessage extends FormattedMessage {

    private static final String MESSAGE_PATTERN = "{} {} - {}";

    RequestMessage(int responseCode, RequestLine requestLine, HttpConnection connection) {
      super(MESSAGE_PATTERN, connection, responseCode, requestLine);
    }
  }
}
