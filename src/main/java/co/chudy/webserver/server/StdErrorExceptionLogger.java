package co.chudy.webserver.server;

import org.apache.http.ConnectionClosedException;
import org.apache.http.ExceptionLogger;

import java.net.SocketTimeoutException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class StdErrorExceptionLogger implements ExceptionLogger {

    private static final Logger logger = LogManager.getLogger(StdErrorExceptionLogger.class);

    @Override
    public void log(final Exception e) {
        if (e instanceof SocketTimeoutException) {
            logger.debug("Connection time out.");
        } else if (e instanceof ConnectionClosedException) {
            logger.debug(e.getMessage(), e);
        } else {
            logger.error(e.getMessage(), e);
        }
    }

}
