package co.chudy.webserver.util;

import java.util.Locale;
import org.apache.http.HttpRequest;

public class HttpMethod {

  private final boolean head;

  private final boolean get;

  public static HttpMethod from(HttpRequest request) {
    String method = request.getRequestLine().getMethod().toUpperCase(Locale.ROOT);
    return new HttpMethod(method);
  }

  private HttpMethod(String method) {
    this.head = method.equals("HEAD");
    this.get = method.equals("GET");
  }

  public boolean isSupported() {
    return get || head;
  }

  public boolean isHead() {
    return head;
  }

}
