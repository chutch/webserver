package co.chudy.webserver.util;

public class MimeTypes {

  public static final String HTML_MIME_TYPE = "text/html";

  public static final String JS_MIME_TYPE = "application/javascript";
}
