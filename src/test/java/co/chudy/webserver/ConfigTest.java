package co.chudy.webserver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class ConfigTest {


  private static final String VALID_ROOT = "src/itest/resources/docroot";
  private static final String VALID_PORT = "8080";
  private static final String VALID_INDEX = "index.html";

  @Test
  void getPort() {
    Config config = new Config(VALID_PORT, VALID_ROOT, VALID_INDEX);
    assertEquals(8080, config.getPort());
  }

  @Test
  void testPortOutOfRange() {
    Config config = new Config("66666", null, null);
    assertThrows(ValidationException.class, config::validate);
    config = new Config("0", null, null);
    assertThrows(ValidationException.class, config::validate);
  }

  @Test
  void testPortNotANumber() {
    Config config = new Config("ABC", null, null);
    assertThrows(ValidationException.class, config::validate);
  }

  @Test
  void testIndexFileNameContainsIllegalCharacter() {
    Config config = new Config(VALID_PORT, null, "will.do?");
    assertThrows(ValidationException.class, config::validate);
  }

  @Test
  void testIndexFileNameBlank() {
    Config config = new Config(VALID_PORT, null, " ");
    assertThrows(ValidationException.class, config::validate);
  }

}